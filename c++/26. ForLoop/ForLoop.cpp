#include <iostream>

int main()
{
	//For loops with multiple counters
	/*
	int l_iCount{ 5 };

	for (int i = 0, j=5; i < l_iCount, j>0; i++, j--)
	{
		std::cout << i << j << '\n';
	}
	*/

	for (int i = 5; i >= 0; i--)
	{

		for (int j = 1; j <= i; j++)
		{
			std::cout << j;
		}

		std::cout << '\n';
	}
	return 0;
}