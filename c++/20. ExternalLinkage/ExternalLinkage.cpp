#include <iostream>
#include "ExternalLinkage.h"

int main()
{
	// Called because Functions have external linkage by default
	JohnCorner();
	SarahCorner();


	std::cout << "Terminator year:" << m_year << '\n';
	std::cout << "Kill Sarah Corner year:" << m_killSarahCornerYear << '\n';
	std::cout << "Kill John Corner year:" << m_killJohnCornerYear << '\n';
	return 0;
}