#include <iostream>

int main()
{
	std::cout << "int:" << sizeof(int) << "\n";
	std::cout << "float:" << sizeof(float) << "\n";
	std::cout << "char:" << sizeof(char) << "\n";

	int l_iX{ 598749 };

	std::cout << "l_iX:" << sizeof(l_iX) << "\n";

}