#include <iostream>


class Data
{
private:
    int value{};

public:
    void Add(int a_iValue)
    {
        value += a_iValue;
    }

    void Print()
    {
        std::cout << "The Value is " << value << '\n';
    }

    Data()
    {
        value = 0;
    }

    friend void ResetValue(Data& a_data);
};

//Because ResetValue() is a friend of the Value class, 
//it can access the private members of all Value objects.
void ResetValue(Data& a_data)
{
    a_data.value = 0;
}


int main()
{
    Data l_data{};
    l_data.Print();
    l_data.Add(10);
    l_data.Print();
    ResetValue(l_data);

    l_data.Print();
    return 0;
}


