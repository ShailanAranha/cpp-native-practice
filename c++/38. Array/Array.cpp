#include <iostream>

void PrintValues(const int a_arrValue[5]);
void ChangeValues(int a_arrValue[5]);

int main()
{
	
	/*std::string l_arrTimeTravellers[]{"Jonus", "Martha", "Hannah", "Noah", "Eulrick"};

	for (int i = 0; i < std::size(l_arrTimeTravellers) ; i++)
	{
		std::cout << l_arrTimeTravellers[i] << '\n';
	}*/

	int l_arrValues[5]{ 1,2,3,4,5 };

	
	PrintValues(l_arrValues);

	ChangeValues(l_arrValues);

	PrintValues(l_arrValues);
	return 0;
}

void PrintValues(const int a_arrValue[5])
{
	//std::size doesnt work with function parameters
	for (int i = 0; i < 5; i++)
	{
		std::cout << a_arrValue[i] ;
	}
	std::cout << '\n';
}

void ChangeValues( int a_arrValue[5])
{
	a_arrValue[0] = 0;
	a_arrValue[1] = 0;
	a_arrValue[2] = 0;
	a_arrValue[3] = 0;
	a_arrValue[4] = 0;
	std::cout << '\n';
}