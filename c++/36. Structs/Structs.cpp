#include <iostream>

struct Dark
{
	std::string Name{};
	int Year{};
	bool IsTimeTraveler{};
};

int main()
{
	Dark AdamsWorld{};
	AdamsWorld.Name=  "Jonus" ;
	AdamsWorld.Year= 1987 ;
	AdamsWorld.IsTimeTraveler= true ;

	std::cout << AdamsWorld.Name << ',' << AdamsWorld.Year << ',' << AdamsWorld.IsTimeTraveler<<'\n';

	Dark EvasWorld{ "Martha", 1954, true };
	//Dark EvasWorld = Dark{ "Martha", 1954, true }; //same as
	// Dark EvasWorld = { "Martha", 1954, true }; //same as
	std::cout << EvasWorld.Name << ',' << EvasWorld.Year << ',' << EvasWorld.IsTimeTraveler << '\n';


	std::cout << sizeof(EvasWorld) << '\n';

	return 0;
}