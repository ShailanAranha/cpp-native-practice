#include <iostream>

int main()
{

	/*int l_iNum{ 594 };

	std::cout << l_iNum << '\n';
	std::cout << &l_iNum << '\n';
	std::cout << *(&l_iNum) << '\n';*/


	int l_iNum{ 594 };
	int* ptr{ &l_iNum };

	std::cout << l_iNum << '\n';
	std::cout << ptr << '\n';
	std::cout << *ptr << '\n';

	*ptr = 639;
	int l_iNum2{ *ptr };

	std::cout << l_iNum2 << '\n';
	std::cout << ptr << '\n';
	std::cout << *ptr << '\n';
	return 0;
}