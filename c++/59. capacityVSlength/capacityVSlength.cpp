

#include <iostream>
#include <vector>

int main()
{
	std::vector<int> array;

	array = { 1,2,5,3,4 };

	std::cout << "Capacity:" << array.capacity() << '\n';
	std::cout << "Length:" << array.size() << '\n';

	array = { 1,2,5 };

	std::cout << "Capacity:" << array.capacity() << '\n';
	std::cout << "Length:" << array.size() << '\n';

	std::cout << '\n';
	std::cout << '\n';

	array.push_back(5);

	std::cout << "Capacity:" << array.capacity() << '\n';
	std::cout << "Length:" << array.size() << '\n';

	std::cout << '\n';
	std::cout << '\n';

	array.pop_back();

	std::cout << "Capacity:" << array.capacity() << '\n';
	std::cout << "Length:" << array.size() << '\n';

	return 0;
}
