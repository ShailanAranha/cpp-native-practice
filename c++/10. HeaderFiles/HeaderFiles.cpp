#include <iostream>
#include "Calculator.h"
#include "Calculator2.h" // header guard prevnets from second declaraltion of Calculator.h


int main()
{
	float l_fltNum1{ 0 };
	float l_fltNum2{0};

	std::cout << "Enter first number:";
	std::cin >> l_fltNum1;

	std::cout << "Enter Second number:";
	std::cin >> l_fltNum2;

	float l_fltResult{ add(l_fltNum1,l_fltNum2) };

	std::cout << "The addition is " << l_fltResult;
}


