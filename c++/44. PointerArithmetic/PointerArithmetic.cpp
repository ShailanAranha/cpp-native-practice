
#include <iostream>

void ArrayPointers();
void ArrayArrangements();

int main()
{

	//int Num{ 10 };

	//int* ptr{ &Num };

	//std::cout << ptr - 2 << '\n';
	//std::cout << ptr - 1 << '\n';
	//std::cout << ptr << '\n';
	//std::cout << ptr + 1 << '\n';
	//std::cout << ptr + 2 << '\n';

	//ArrayPointers();

	ArrayArrangements();


    return 0;
}

/// <summary>
/// Arrays are laid out sequentially in memory
/// </summary>
void ArrayPointers()
{
	bool IsInteractible[]{ false, true, false, true };

	//std::cout << IsInteractible << '\n';

	std::cout << &IsInteractible[0] << '\n';
	std::cout << &IsInteractible[1] << '\n';
	std::cout << &IsInteractible[2] << '\n';
	std::cout << &IsInteractible[3] << '\n';
}

/// <summary>
/// Array Arrangements in the memory 
/// </summary>
void ArrayArrangements()
{
	bool IsInteractible[]{ false, true, false, true };

	//std::cout << IsInteractible << '\n'; //ARRAY POINTER (as array decay into pointer)


	std::cout << IsInteractible[0] << " = " << *(IsInteractible) << '\n';
	std::cout << IsInteractible[1] << " = " << *(IsInteractible + 1) << '\n';
	std::cout << IsInteractible[2] << " = " << *(IsInteractible + 2) << '\n';
	std::cout<< IsInteractible[3]<< " = " << *(IsInteractible + 3) << '\n';
	
}
