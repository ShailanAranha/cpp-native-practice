#include <iostream>

int main()
{
	// C-STYLE CAST
	int a{ 2 };

	float a_conversion = float(a);  // or  (float)a;
	
	std::cout << a_conversion <<'\n';

// STATIC CAST

	float b = static_cast<float>(a) / 1.5;
	std::cout << b << '\n';

	return 0;
}