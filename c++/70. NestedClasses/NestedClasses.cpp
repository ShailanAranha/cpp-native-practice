#include <iostream>
#include <string>

class Sitcom
{

public:
	enum TwoNHalfMen
	{
		Charlie = 0,
		Alan=1,
		Jake = 2
	};


	TwoNHalfMen getFirstMen()
	{
		return TwoNHalfMen::Charlie;
	}
};



int main()
{
	Sitcom m_obj = {};

	std::cout << m_obj.getFirstMen() << '\n';
	std::cout << m_obj.Charlie << '\n';
	return 0;
}