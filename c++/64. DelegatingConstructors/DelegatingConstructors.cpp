#include <iostream>

class TwoNHalfMen
{
public: 
	TwoNHalfMen()
	{
		std::cout << "Charlie and Allan ";
	}

	TwoNHalfMen(std::string a_strKid) : TwoNHalfMen{}
	{
		std::cout <<"and " << a_strKid;
	}

};

int main()
{
	TwoNHalfMen l_TwoNHalfMen("Jake");
	return 0;
}

