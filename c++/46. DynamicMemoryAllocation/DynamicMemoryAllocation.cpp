#include <iostream>

void ForArray();

int main()
{
	//int array[1000000];

	// Assign dynamic memory
	int* l_ptrNum{ new int {10} };

	// Use dynamic memory
	int num{ *l_ptrNum };

	std::cout << *l_ptrNum << '\n';
	std::cout << num << '\n';

	// delete dynamic memory
	delete l_ptrNum;
	l_ptrNum = nullptr;

	
	ForArray();

	return 0;
}

void ForArray()
{
	std::cout << "Enter the size:";
	size_t length{0};
	std::cin >> length;

	int* array{ new int[length] {} };

	array[0] = 5;

	delete[] array;




}

