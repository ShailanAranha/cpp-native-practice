#include <iostream>

class Dark
{
private:
	std::string Adam;
	std::string Eva;

	// DEFAULT CONSTRUCTOR
public:
	Dark(std::string a_adam, std::string a_eva) : Adam{ a_adam }, Eva{a_eva}
	{
	}

	// DEFAULT COPY CONTRUCTOR 
	/*Dark(const Dark& a_dark) : Adam{ a_dark.Adam }, Eva{ a_dark.Eva } // CAN ACCESS PRIVATE MEMBERS AS WELL
	{
		std::cout << "Copy Constrctor called" << '\n';
	}*/

	void PrintValue()
	{
		std::cout << "Adam is " << Adam << " and eva is " << Eva << '\n';
	}

	void ChangeAdam(std::string a_adam)
	{
		Adam = a_adam;
	}

	void ChangeEva(std::string a_eva)
	{
		Eva = a_eva;
	}
};

int main()
{
	Dark l_obj1{ "Jonus", "Martha" };
	l_obj1.PrintValue();

	Dark l_obj2{ l_obj1 };
	l_obj2.PrintValue();

	//CHANGE VALUE
	l_obj2.ChangeAdam("Bartosh");

	// CHECK AGAIN
	l_obj1.PrintValue();
	l_obj2.PrintValue();

	return 0;
}