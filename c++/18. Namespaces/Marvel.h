#include <string>

// HEADER GUARD
#ifndef  MARVEL

#define MARVEL

namespace marvel_universe
{
	std::string WhichUniverse();
}
#endif // ! MARVEL

