#include <string>

// HEADER GUARD
#ifndef  DC
#define DC

namespace dc_universe
{
	std::string WhichUniverse();
}

#endif // ! DC

