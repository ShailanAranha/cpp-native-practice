#include <iostream>
#include "Marvel.h";
#include "DC.h";

namespace marvel
{
	void PowerfulHero()
	{
		std::cout << "IRON MAN!!";
	}
}

namespace dc
{
	void PowerfulHero()
	{
		std::cout << "SUPERMAN!!";
	}
}

// EXPLICIT CALL TO GLOBAL NAMESPACE
void PowerfulHero()
{
	std::cout << "THANOS!!";
}

namespace dark::jonus
{
	void aim()
	{
		std::cout << "EVERYTHING IS CONNECTED";
	}
}


// DECLARATION
namespace doctorwho
{
	void PowerfulHero();
}

int main()
{

	// SAME FILE
	std::cout <<  "SAME FILE:" << '\n';

	marvel::PowerfulHero();
	std::cout << '\n';
	dc::PowerfulHero();
	std::cout << '\n';
	doctorwho::PowerfulHero();

	std::cout << '\n';

	// EXPLICIT CALL TO GLOBAL NAMESPACE
	std::cout << "\n\EXPLICIT CALL TO GLOBAL NAMESPACE" << '\n';
	PowerfulHero();
	std::cout << '\n';
	::PowerfulHero();  // :: without namespace is to explicitly call to the global namespace



	// DIFFERENT FILE
	std::cout << "\n\nDIFFERENT FILE:" << '\n';
	std::cout << dc_universe::WhichUniverse() << '\n';
	std::cout<< marvel_universe::WhichUniverse() << '\n';

	// NESTED NAMESPACE
	std::cout << "\n\nNESTED NAMESPACE:" << '\n';
	dark::jonus::aim();

	//Namespace aliases
	std::cout << "\n\nNamespace aliases:" << '\n';
	namespace timeTravel = dark::jonus;
	timeTravel::aim();

	std::cout << "\n\n\n";

	return 0;
}

namespace doctorwho
{
	void PowerfulHero()
	{
		std::cout << "Matt smith!!";
	}
}