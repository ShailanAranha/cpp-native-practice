#include <iostream>

class Sitcom
{
private:
	std::string Name;

public:
	Sitcom(std::string a_name = "dARK") : Name{a_name}
	{
		std::cout << Name << '\n';
	}
};

int main()
{
	// DIRECT INITIAZATION
	Sitcom l_sitcom1("Two and a half men - DIRECT INITIAZATION");

	// UNIFORM INITIALIZATION
	Sitcom l_sitcom2{"Two and a half men - UNIFORM INITIAZATION"};

	// COPY INITIAZATION
	Sitcom l_sitcom3 = Sitcom{ "Two and a half men - COPY INITIAZATION" };

	return 0;
}