#include <iostream>

int main()
{
	 int l_arrNumbers[]{0,1,2,3,4,5,6,7};

	for (int num:l_arrNumbers)
	{
		std::cout << num;
	}

	// For optimization
	std::cout << '\n';

	for (int& num : l_arrNumbers)
	{
		std::cout << num;
	}
	return 0;
}