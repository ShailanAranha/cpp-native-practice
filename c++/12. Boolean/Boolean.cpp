#include <iostream>

int main()
{

	// PRINT BOOL
	/*
	bool l_isVar1{ true };
	bool l_isVar2{ false};

	std::cout << l_isVar1 << '\n';
	std::cout << l_isVar2 << '\n';;

	std::cout<< std::boolalpha;

	std::cout << l_isVar1 << '\n';;
	std::cout << l_isVar2<< '\n';;

	*/

	// BOOL INTEGER COMPARISION
	/*
	bool l_b1= 1 ;
	bool l_b2= 0;
	bool l_b3= 85 ;

	std::cout << std::boolalpha;

	std::cout << l_b1 << '\n';
	std::cout << l_b2 << '\n';
	std::cout << l_b3 << '\n';
	*/


	// INPUT BOOLS

	//BOOL CAN ONLY TAKE 0 OR 1 AS INPUT

	bool l_input1{ false };
	bool l_input2{ false };

	std::cout << "Enter 0 or 1" << '\n';

	std::cin >> l_input1;

	std::cout << l_input1 << '\n';


	std::cin >> std::boolalpha;
	std::cout << "Enter true or false" << '\n';

	std::cin >> l_input2;

	std::cout << l_input2 << '\n';
	std::cout << std::boolalpha;
	std::cout << l_input2 << '\n';

	return 0;

}