#include <iostream>

void Example();

int main()
{
    //Example();

    int l_iInput{ 0 };
    std::cin >> l_iInput;

    switch (l_iInput)
    {
        int l_iOutput;
    case 1:
       // int l_iOutput1{};
        l_iOutput = 1;
        break;

    case 2:
        l_iOutput = 2;

        break;

    case 3:
        l_iOutput = 3;

        break;

    default:
        l_iOutput = 0;

        break;

    std::cout << l_iOutput;
    }


	return 0;
}

void Example()
{
    int l_iInput{ 0 };
    std::cin >> l_iInput;

        switch (l_iInput)
        {
            int a; // okay: declaration is allowed before the case labels
            int b{ 5 }; // illegal: initialization is not allowed before the case labels

        case 1:
            int y; // okay but bad practice: declaration is allowed within a case
            y = 4; // okay: assignment is allowed
            break;

        case 2:
            y = 5; // okay: y was declared above, so we can use it here too
            break;

        case 3:
            int z{ 4 }; // illegal: initialization is not allowed within a case
            break;
        }
}