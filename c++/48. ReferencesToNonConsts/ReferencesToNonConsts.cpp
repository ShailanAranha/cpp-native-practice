#include <iostream>

void ReferenceTest();

int main()
{
	ReferenceTest();

	return 0;
}

void ReferenceTest()
{
	int obj1{ 10 };

	int& obj2{ obj1 };

	std::cout << "obj1:" << obj1 <<'\n';
	std::cout << "obj2:" << obj2 << '\n';
	obj2 = 20;
	std::cout << "obj1:" << obj1<<'\n';
	std::cout << "obj2:" << obj2 << '\n';

	std::cout << "obj1 Address:" << &obj1 << '\n';
	std::cout << "obj2 Address:" << &obj2 << '\n';
}

void ReferenceTest2()
{
	
}