#include <iostream>

// FORWARD DECLARATION
void PrintSalary(int a_iAmount);

int main()
{
	PrintSalary(100);

	return 0;
}

// DEFINATION
void PrintSalary(int a_iAmount) 
{
	std::cout << "My Salary is " << a_iAmount;
}