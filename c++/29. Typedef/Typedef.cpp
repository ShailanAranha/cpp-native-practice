#include <iostream>

int main()
{
	typedef float speed_t;
	typedef float acceleration_t;

	speed_t max{ 100 };
	speed_t min{ 0 };


	speed_t playerSpeed{ 0.0f };

	while (true)
	{
	
		std::cout << "Enter speed:";
		std::cin >> playerSpeed;

		if (playerSpeed)

			if (playerSpeed < min)
			{
				std::cout << "Too slow" << '\n';
			}
			else if (playerSpeed > max)
			{
				std::cout << "Too fast" << '\n';
			}
			else
			{
				std::cout << "Game on" << '\n';
				break;
			}
	}


	return 0;
}