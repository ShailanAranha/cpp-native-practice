#include <iostream>

class TwoNHalfMen
{
	public:

	void GetFirstMen() const
	{
		std::cout << "This is Charlie" << '\n';
	}

};

void CallMen(const TwoNHalfMen a_menObj);

int main()
{
	const TwoNHalfMen l_menObj{};
	CallMen(l_menObj);
	return 0;
}

void CallMen(const TwoNHalfMen a_menObj)
{
	a_menObj.GetFirstMen();
}