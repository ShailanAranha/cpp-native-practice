#include <iostream>

int Add(int a_iNum1, int a_iNum2)
{
	return a_iNum1 + a_iNum2;
}

int Subtract(int a_iNum1, int a_iNum2)
{
	return a_iNum1 - a_iNum2;
}

int Multiply(int a_iNum1, int a_iNum2)
{
	return a_iNum1 * a_iNum2;
}

int main()
{
	int l_iNum1{ 0 };
	int l_iNum2{ 0 };

	std::cout << "Enter first number:";
	std::cin >> l_iNum1;

	std::cout << "Enter second number:";
	std::cin >> l_iNum2;

	int l_iResult{ Add(l_iNum1, l_iNum2) };

	std::cout << "Addition of "<< l_iNum1 <<" and "<< l_iNum2<< " is " << l_iResult;

	return 0;
}