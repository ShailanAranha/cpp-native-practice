#include <iostream>

int GenerateUniqueId();

int main()
{
	std::string l_strInput{ };
	std::cout << "Press 0 to exit the program: \n";
	   
	while (true)
	{
		std::cin>> l_strInput;
		 
		if (l_strInput == "0")
		{
			break;
		}

		std::cout << GenerateUniqueId();
	}

	return 0;
}


int GenerateUniqueId()
{
	static int l_iID{ 0 }; //WILL WE INITAILIZE ONCE AND WILL RETAIN ITS VALUE EVEN AFTER ITS SCOPE ENDS
	return l_iID++;
}