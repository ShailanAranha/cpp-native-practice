#include <iostream>
#include <vector>

int main()
{
	/// DFINATION
	std::vector<int> l_vecList1;
	/// INITIALIZATION TYPES
	std::vector<int> l_vecList2= { 4,5,6 };
	std::vector<int> l_vecList3{ 4,5,6 };
	std::vector l_vecList4{ 4,5,6 };

	// SIZE CHECK
	std::cout << l_vecList2.size() << '\n';

	// RESIZE

	for (int num : l_vecList4)
	{
		std::cout << num ;
	}

	std::cout << '\n';
	l_vecList4.resize(10);

	for (int num : l_vecList4)
	{
		std::cout << num ;
	}

	std::cout << '\n';

	//COMPACTING BOOLS

	std::vector<bool> l_vecBoolean{ true, false,true, false ,true, false ,true };

	std::cout << "LENGTH:" << l_vecBoolean.size() << '\n';
	std::cout << "TOTAL SIZE:"<< sizeof(l_vecBoolean) << '\n';

}
