#include <iostream>

int main()
{
	int l_array[]{ 6, 3, 2, 9, 7, 1, 5, 4, 8 };

	int l_iCount{ std::size(l_array) };

	int counter{ 0 };

	while (counter<l_iCount-2)
	{
		for (int i = 0+ counter; i < l_iCount-1; i++)
		{
			if (l_array[i] > l_array[i + 1])
			{
				int temp{ l_array[i] };
				l_array[i] = l_array[i + 1];
				l_array[i + 1] = temp;
			}
		}

		counter++;
	}

	for (int i = 0; i < l_iCount; i++)
	{
		std::cout << i;
	}

	return 0;
}