#include <iostream>

float calculateTenPercent(int a_iInput);

int main()
{

	int l_iInput{0};
	std::cout << "ENTER A NUMBER FOR CALCULATION:";
	
	std::cin >> l_iInput;

	float l_iTenPercent{ calculateTenPercent(l_iInput) };

	std::cout << "The 10% of "<< l_iInput<<" is "<< l_iTenPercent;

	return 0;
}
