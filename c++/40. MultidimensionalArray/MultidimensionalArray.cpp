#include <iostream>

int main()
{

	int l_2dArray[3][5]{
		{1,5,9,6,3}, // row 1
		{5,8,9,6,4}, // row 2
		{3,5,7,1,9}  // row 3
	};

	int l_2dArray1[][2]{ {1,2},{3,9}, {5,8} };

	std::cout << "Array size:" << std::size(l_2dArray) << '\n';
	std::cout << "Array size:"<<std::size(l_2dArray[01]) << '\n';

	for (int i = 0; i < std::size(l_2dArray); i++)
	{
		for (int j = 0; j < std::size(l_2dArray[i]); j++)
		{
			std::cout << l_2dArray[i][j] << '\n';
		}
	}

	return 0;
}