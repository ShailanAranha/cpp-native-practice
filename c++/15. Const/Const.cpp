#include <iostream>

#include "Constants.h"

void myMethod(const int a_value);

int main()
{
	// COMPILE TIME CONSTANTS

	/*
	const int NUMBER = 1; // prefered
	int const  NUMBER1 = 1;

	constexpr int Number2 = 1564; // constexpr ensures that a constant must be a compile-time constant:

	myMethod(10);
	*/


	// RUNTIME TIME CONSTANTS
	/*

	std::cout << "ENTER USER'S AGE: ";
	int l_iAge{ 0 };

	std::cin >> l_iAge;

	const int USER_AGE = l_iAge;

	std::cout << "User age is " << USER_AGE;
	*/

	// NAMESPACE

	std::string Name{ Constants::NAME };
	int Age{ Constants::AGE };
	std::string Profession{ Constants::PROFESSION };

	std::cout << "User details: " << Name<< ", "<< Age<<", "<< Profession;

	return 0;
}

void myMethod(const int a_value)
{
	//a_value = 1;
	std::cout << a_value << '\n';
}