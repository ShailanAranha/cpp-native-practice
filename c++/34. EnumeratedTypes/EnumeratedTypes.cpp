
// DO NOT USE IT, USE ENUM CLASS INSTEAD
#include <iostream>

enum Marvel
{
	Avengers,
	XMen
};

enum DC :bool
{
	Superman,
	Batman
};

int main()
{
	Marvel Etype{ Avengers };


	std::cout << Etype << '\n';
	std::cout << static_cast<Marvel>(Etype) << '\n';


	Marvel avenger{ static_cast<Marvel>(0) };


	DC Edc{ DC::Batman };


	std::cout << Edc << '\n';
}

