#include <iostream>

enum class Dark
{
	Jonus, Martha
};

enum class Krad
{
	Adam, Eva
};

int main()
{
	// THIS WONT WORK AS ENUM CLASSES DOESNT IMPLICITLY COMPARE IT WITH INTEGER
	//std::cout << Dark::Jonus << '\n';
	Dark orgin{ Dark::Jonus };

	if(orgin == Dark::Martha)
		std::cout << "Martha knew about orgin" << '\n';
	else
		std::cout << "Jonus knew about orgin" << '\n';


	return 0;
}

