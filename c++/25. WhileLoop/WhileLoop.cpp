#include <iostream>

int main()
{
		/*
	int l_iCounter{ 5 };

	while (l_iCounter>0)
	{
		int l_iInnerCounter{ l_iCounter };

		while (l_iInnerCounter > 0)
		{
			std::cout << l_iInnerCounter;
			--l_iInnerCounter;
		}

		std::cout << '\n';
		--l_iCounter;
	}
*/

	int l_iCounter{ 1 };

	while (l_iCounter <= 5)
	{
		int l_iInnerCounter{ 0 };

		int temp{ l_iCounter };

		while (l_iInnerCounter <= (5- temp))
		{
			std::cout << " ";
			++l_iInnerCounter;
		}

		while (temp > 0)
		{
		std::cout << temp;
		--temp;
		}

		std::cout << '\n';
		++l_iCounter;
	}

	return 0;
}