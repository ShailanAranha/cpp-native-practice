#include <iostream>

class Pound
{
private:
		int m_iPound = 0;

public :
		int getPound()
		{
			return m_iPound;
		}

		Pound(int a_pound)
		{
			m_iPound = a_pound;
		}

		friend Pound operator+(Pound a_one, Pound a_two);
};

Pound operator+(Pound a_one, Pound a_two)
{
	return Pound(a_one.m_iPound + a_two.m_iPound);
}

int main()
{
	Pound Taxi{26};
	Pound Food{ 120 };
	Pound Rent{ 350 };
	Pound CovidTest{ 99 };

	Pound l_totalQurantineExpense = Taxi + Food + Rent+ CovidTest;

	std::cout << "Total qurantine expense is " << l_totalQurantineExpense.getPound() << '\n';
	return 0;
}