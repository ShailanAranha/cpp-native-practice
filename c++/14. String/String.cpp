#include <iostream>
#include <iomanip>
#include <string>

int main()
{

	//std::string l_strName{ "Optimus Prime" };

	//std::cout << l_strName;

	// CIN

	std::string l_strName{};
	std::string l_strModel{};

	std::cout << "Enter the name of the Transformer:";

	std::getline(std::cin >> std::ws, l_strName);

	std::cout << "Enter the name of the Terminator:";

	std::getline(std::cin >> std::ws, l_strModel);

	std::cout << "Transformer:" << l_strName << " Terminator:" << l_strModel;

	return 0;
}