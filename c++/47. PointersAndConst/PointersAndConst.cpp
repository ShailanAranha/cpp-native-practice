#include <iostream>

void ConstantPointers();

int main()
{
	const int value{ 20 };

//Here the pointer is non-constant but it treats the value its holding as constant so cannot change the value
	const int* ptr1{ &value };  // value can or cannot be constant
	//*ptr = 6; // can't as the ptr treats it value as constant

	ConstantPointers();
	return 0;
}

/// <summary>
/// Here the pointer is constant but value can be changed
/// </summary>
void ConstantPointers()
{
	int value{ 10 };
	int value2{ 30 };
	int* const ptr{ &value }; // now ptr cannot be changed after initialization

	std::cout << value << '\n';

	//ptr = &value2; // cannot reassign a new address
	value = 50; // BUT CAN CHANGE VALUE
	std::cout << value << '\n';
}
