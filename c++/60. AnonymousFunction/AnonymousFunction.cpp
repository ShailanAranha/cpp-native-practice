#include <iostream>
#include <functional>

void Function(std::function<int(int num1, int num2)> fcn);

int main()
{
	//Anonymous function
	
	auto result
	{
		[](int num1, int num2)
		{
			return num1 + num2;
		}
	};


	Function(result);

	return 0;
}

void Function(std::function<int (int num1, int num2)> fcn)
{
	int num = fcn(5,10);

	std::cout << num << '\n';
}

