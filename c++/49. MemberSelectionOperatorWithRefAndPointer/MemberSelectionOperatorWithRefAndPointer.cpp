#include <iostream>

struct Dark
{
	std::string YoungerSelf{};
	std::string OlderSelf{};
};

int main()
{

	// USING REFERENCE

	Dark World1{};
	Dark& ref{ World1 };

	ref.OlderSelf = "Adam";
	ref.YoungerSelf = "Jonus";

	std::cout << "OlderSelf:" << World1.OlderSelf << " YoungerSelf:" << ref.YoungerSelf << '\n';

	// USING POINTER

	Dark World2{};
	Dark* ptr{&World2};

	ptr->OlderSelf = "Eva";
	ptr->YoungerSelf = "Martha";

	std::cout << "OlderSelf:" << World2.OlderSelf << " YoungerSelf:" << ptr->YoungerSelf << '\n';


	return 0;
}