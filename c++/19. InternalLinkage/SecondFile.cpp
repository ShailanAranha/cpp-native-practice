#include <iostream>

void JohnCorner();
void SarahCorner();

/// this extern is a forward declaration of a variable named m_iNum that is defined somewhere else
extern int m_iNum;


void AccessNumber()
{
	// CANNOT BE ACCESSED AS THIS VARIABLE IS INTERNAL VARIABLE (INTERNAL LINKAGE)
	std::cout << m_iNum;

	// CAN BE ACCESSED AS ALL FUNCTIONS FOLLOWS EXTRENAL LINKAGE BY DEFAULT
	SarahCorner();

	// CANNOT BE ACCESSED AS THIS FUNCTIONS FOLLOWS INTERNAL LINKAGE (DEFINED AS STATIC)
	//JohnCorner();
}