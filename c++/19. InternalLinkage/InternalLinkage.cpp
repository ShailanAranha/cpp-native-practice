#include <iostream>

void AccessNumber();
void JohnCorner();
void SarahCorner();


static int m_iNum{ 10 };

int main()
{

	std::cout << m_iNum << '\n';
	AccessNumber();

	JohnCorner();
	SarahCorner();

	return 0;
}

//void AccessNumber()
//{
//	std::cout << "12";
//}

static void JohnCorner()
{
	std::cout << "JohnCorner" << '\n';
}

void SarahCorner()
{
	std::cout << "SarahCorner" << '\n';
}