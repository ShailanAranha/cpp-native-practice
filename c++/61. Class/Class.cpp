

#include <iostream>

class Dark
{
public:
    std::string m_name = "";

    void RevealName()
    {
        std::cout << m_name << '\n';
    }
};

int main()
{
    Dark jonus{ "Adam" };
    jonus.RevealName();

    Dark martha{ "Eva" };
    martha.RevealName();
}



