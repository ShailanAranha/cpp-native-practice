#include <iostream>
#include <cmath>

bool approximatelyEqual(double a, double b, double epsilon);
bool approximatelyEqualAbsRel(double a, double b, double absEpsilon, double relEpsilon);


int main()
{
	float l_fltNum1{ 1.0 + 1.0 };
	float l_fltNum2{ 2.0 };

    if (approximatelyEqualAbsRel(l_fltNum1, l_fltNum2, 1e-12, 1e-8))
    {
        std::cout << "Equal";
    }
    else
    {
        std::cout << "In Equal";
    }

	return 0;
}

// return true if the difference between a and b is within epsilon percent of the larger of a and b
bool approximatelyEqual(double a, double b, double epsilon)
{
	return (std::abs(a - b) <= (std::max(std::abs(a), std::abs(b)) * epsilon));
}

bool approximatelyEqualAbsRel(double a, double b, double absEpsilon, double relEpsilon)
{
    // Check if the numbers are really close -- needed when comparing numbers near zero.
    double diff{ std::abs(a - b) };
    if (diff <= absEpsilon)
        return true;

    // Otherwise fall back to Knuth's algorithm
    return (diff <= (std::max(std::abs(a), std::abs(b)) * relEpsilon));
}
