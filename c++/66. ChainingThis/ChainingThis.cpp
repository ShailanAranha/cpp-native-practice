#include <iostream>

class TwoNHalfMen
{
public:
	TwoNHalfMen& FirstMen()
	{
		std::cout << "Charlie" << "\n";

		return *this;
	}

	TwoNHalfMen& SecondMen()
	{
		std::cout << "Alan" << "\n";

		return *this;
	}

	TwoNHalfMen& HalfMen()
	{
		std::cout << "Jake"<<"\n";

		return *this;
	}

};

int main()
{
	TwoNHalfMen l_obj = {};

	l_obj.FirstMen().SecondMen().HalfMen();
	return 0;
}