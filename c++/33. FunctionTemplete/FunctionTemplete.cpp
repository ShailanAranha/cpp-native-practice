#include <iostream>

template <typename T>
T max(T a_Num1, T a_Num2);

template <typename T, typename U>
T Add(T a_Num1, U a_Num2);

template <typename T, typename U>
auto subtract(T a_Num1, U a_Num2)
{
	return a_Num1 - a_Num2;
}

int main()
{
	
	std::cout << max(5, 3) << '\n';
	std::cout << max(5.3, 3.5) << '\n';
	std::cout << max(7.3f, 13.5f)<<'\n';


	std::cout << max(7.3f, 13.5f) << '\n';


	std::cout << Add<double, float>(7.3, 13.5f) << '\n';

	std::cout << subtract<int, float>(7, 13.5f) << '\n';


	return 0;
}


template <typename T>
T max(T a_Num1, T a_Num2) 
{
	return (a_Num1 > a_Num2) ? a_Num1 : a_Num2;
}

template <typename T, typename U>
T Add(T a_Num1, U a_Num2)
{
	return a_Num1 + a_Num2;
}

