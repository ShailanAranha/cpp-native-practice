#include <iostream>
#include <tuple>

std::tuple<int, double> getValues();

int main()
{
	std::tuple<int, double> value{ getValues() };

	std::cout << std::get<0>(value) << " " << std::get<1>(value) << '\n';

	// using tie

	int a; 
	double b;

	std::tie(a, b) = getValues() ;

	std::cout << a << " " << b << '\n';


	return 0;
}


std::tuple<int, double> getValues()
{
	return { 1, 5.6 };
}
