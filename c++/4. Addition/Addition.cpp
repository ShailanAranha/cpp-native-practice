#include <iostream>

int main()
{

	std::cout << "Enter two numbers to add:\n";

	int m_iNum1{ 0 };
	int m_iNum2{ 0 };
	
	std::cin >> m_iNum1;
	std::cin >> m_iNum2;

	int m_iResult{ m_iNum1 + m_iNum2 };
	
	std::cout << "The addition of the " << m_iNum1 << " and " << m_iNum2 << " is " << m_iResult;
	
	return 0;
}