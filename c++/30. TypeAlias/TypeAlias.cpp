#include <iostream>

int main()
{
	using acceleration_t = float;

	acceleration_t max{ 100 };
	acceleration_t min{ 0 };


	acceleration_t playerAcceleration{ 0.0f };

	while (true)
	{

		std::cout << "Enter acceleration_:";
		std::cin >> playerAcceleration;

		if (playerAcceleration)

			if (playerAcceleration < min)
			{
				std::cout << "Too slow" << '\n';
			}
			else if (playerAcceleration > max)
			{
				std::cout << "Too fast" << '\n';
			}
			else
			{
				std::cout << "Game on" << '\n';
				break;
			}
	}

	using string_t = std::string;

	string_t input{};
	std::cout << "Enter a text" << '\n';

	std::cin >> input;

	std::cout << input << '\n';

	return 0;
}