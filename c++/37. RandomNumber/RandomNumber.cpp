#include <iostream>
#include <cstdlib> // for std::rand() and std::srand()

int generateRandomNumber(int seed)
{
	static int randomNum{ 2563 };

	 randomNum=  seed % randomNum;
	return randomNum;
}

int main ()
{
	/*int counter{ 0 };

	while (counter < 1)
	{
		std::cout << generateRandomNumber(5634) <<'\n';
		counter++;
	}*/

	//std::srand(5323);
	std::srand(static_cast<unsigned int>(std::time(nullptr))); // set initial seed value to system clock

	//std::rand();


	int counter{ 0 };
	while (counter < 10)
	{
		std::cout << std::rand() << '\n';
		counter++;
	}

	return 0;
}