#include <iostream>

class Alan
{

public:
	std::string Son{ "Jake" };

	friend class Charlie;
};

class Charlie
{
public:
	void Nephew(const Alan& a_alan)
	{
		std::cout << a_alan.Son << '\n';
	}
};

int main()
{
	Alan l_alan{};
	Charlie l_Charlie{};
	l_Charlie.Nephew(l_alan);
	return 0;
}

