#include <iostream>

void UsingDeclaration();
void UsingDirective();

int main()
{
	UsingDeclaration();

	UsingDirective();
	return 0;
}

void UsingDeclaration()
{
	using std::cout;
	using std::cin;
	using std::string;

	string l_strInput{};
	cout << "Using Statement Declaration, Input:";
	cin >> l_strInput;
	cout << "Input:" << l_strInput << '\n';
}

void UsingDirective()
{
	using namespace std;

	string l_strInput{};
	cout << "Using Statement directive, Input:";
	cin >> l_strInput;
	cout << "Input:" << l_strInput << '\n';
}