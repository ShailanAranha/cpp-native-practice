#include <iostream>

int main()
{

	// STATIC CAST
		
	char l_char1{ '5' };
	char l_char2{'a'};

	int l_iNum1 = static_cast<int>(l_char1);
	int l_iNum2 = static_cast<int>(l_char2);

	std::cout << l_iNum1 << '\n';
	std::cout << l_iNum2 << '\n';
	return 0;
}