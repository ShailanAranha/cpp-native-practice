
#include <iostream>
#include <functional>

int NineNine();
double OneOne();
int FiveFive(int a_num);

void sort(int* array, int a_size, bool (*comparision)(int, int));
bool Ascending(int a_num1, int a_num2);
bool Descending(int a_num1, int a_num2);



int main()
{

	int (*fcnPtr)() { &NineNine };

	double (*fncPtr1)() { &OneOne };

	int (*fcnPtr2)(int) { &FiveFive };

	// TWO WAYS OF CALLING
	std::cout <<"TWO WAYS OF CALLING" << '\n';

	// EXPLICIT dereference

	//std::cout << (*fcnPtr)() << '\n';

	int l_iNUm = (*fcnPtr)();

	std:: cout << l_iNUm <<'\n';

	// IMPLICIT dereference

	//std::cout << fcnPtr2(10) << '\n';

	int l_iNUm2{ fcnPtr2(10) };

	std::cout << l_iNUm2 << '\n';


	// PASSING FUNCTIONS INSIDE FUNCTIONS
	std::cout << "PASSING FUNCTIONS INSIDE FUNCTIONS" << '\n';

	int array[]{ 2,6,8,3,4,66,8 };
	
	sort(array, 7, Ascending);

	sort(array, 7, Descending);

	// DEFAULT PARAMETER

	// Using std::function

	//int (*fcnPtr)() { &NineNine }; ALTERNATE METHOD
	std::function<int()> fcnPtr3{ &NineNine };

	//TYPE ALIAS IS A BETTER CHOICE
	using FunctionPointer = std::function<int()>;
	FunctionPointer fcnPtr4{ &NineNine };

	return 0;
}


int NineNine()
{
	return 99;
}

double OneOne()
{
	return 1.1;
}

int FiveFive(int a_num)
{
	return a_num;
}

void sort(int* array, int a_size, bool (*comparision)(int, int))
{

	for (int i = 0; i < a_size; i++)
	{
		
		for (int  j = 0;  j < i+1;  j++)
		{
			if ((*comparision)(array[i], array[j]))
			{
				int temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
		}
	}

	for (int index{ 0 }; index < a_size; ++index)
	{
		std::cout << array[index] << ' ';
	}

	std::cout << '\n';
}

bool Ascending(int a_num1, int a_num2)
{
	return a_num1 < a_num2;
}

bool Descending(int a_num1, int a_num2)
{
	return a_num1 > a_num2;
}

void sortUsingStdFunction(int* array, int a_size, std::function<bool(int, int)> comparision)
{

	for (int i = 0; i < a_size; i++)
	{

		for (int j = 0; j < i + 1; j++)
		{
			if (comparision(array[i], array[j]))
			{
				int temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
		}
	}

	for (int index{ 0 }; index < a_size; ++index)
	{
		std::cout << array[index] << ' ';
	}

	std::cout << '\n';
}

