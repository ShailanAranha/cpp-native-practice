#include <iostream>

class Sitcom
{
private:
	std::string m_strName{};
	const int m_iTotalCharacers{};

	// NORMAL METHOD
public:
	Sitcom(std::string a_strName, int a_iTotalName)
	{
		// These are all assignments, not initializations
		m_strName = a_strName;
		//m_iTotalCharacers = a_iTotalName;
	}

	//MEMBER INITIALIZATION LIST
public:
	Sitcom(std::string a_strName, int a_iTotalName) :m_strName{ a_strName }, m_iTotalCharacers{a_iTotalName} // this is initialization
	{
		// No need for assignment here
	}

};

int main()
{
   
}
