#include <iostream>

const int& ReturnConstReference();

int main()
{

	const int& getValue{ ReturnConstReference() };


	std::cout << getValue << '\n';

	return 0;
}


const int& ReturnConstReference()
{
	return 5;
}

