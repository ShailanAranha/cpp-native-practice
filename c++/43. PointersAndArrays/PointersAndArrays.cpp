#include <iostream>

// Both function are identical
void Function1(int array[]);
void Function2(int* array);

int main()
{
	int array[]{ 5,8,9,3,6,7,9,2 };

	int* ptr{ array };

	std::cout << "The pointer to the first element of the array " << &array[0] << '\n';
	std::cout << "The pointer point to array " << ptr << '\n';
	
	
	std::cout << "The size of array " << sizeof(array) << '\n';
	std::cout << "The size of pointer " << sizeof(ptr)<<'\n';

	std::cout << "The address of array " << &array << '\n';
	std::cout << "The address of pointer " << &ptr << '\n';

	Function1(array);
	Function2(array);

	return 0;
}

void Function1(int array[])
{
	std::cout << "size " << sizeof(array) << '\n';
}

void Function2(int* array)
{
	std::cout << "size " << sizeof(array) << '\n';
}