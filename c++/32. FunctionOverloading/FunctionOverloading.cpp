#include <iostream>

int Add(int a_iNum1, int a_iNum2);
int Add(int a_iNum1, int a_iNum2, int a_iNum3);
float Add(float a_iNum1, float a_iNum2);

int main()
{
	std::cout << Add(5, 3) << '\n';
	std::cout << Add(5.3f, 3.5f) << '\n';
	std::cout << Add(5, 3,25) << '\n';

	return 0;
}

int Add(int a_iNum1, int a_iNum2)
{
	return a_iNum1 + a_iNum2;
}

int Add(int a_iNum1, int a_iNum2, int a_iNum3)
{
	return a_iNum1 + a_iNum2+ a_iNum3;
}

float Add(float a_iNum1, float a_iNum2)
{
	return a_iNum1 + a_iNum2;
}