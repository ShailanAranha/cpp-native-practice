#include <iostream>

// unnamed namespaces, which implicitly treat all contents of the namespace as if it had internal linkage.
namespace
{
	void JohnCorner()
	{
		std::cout << "I'm john corner" << '\n';;
	}
}

//line namespaces, which provide some primitive versioning capabilities for namespaces.
inline namespace Terminator1
{
	void SarahCorner()
	{
		std::cout << "I'm sarah corner from Terminator1" << '\n';
	}
}

namespace Terminator2
{
	void SarahCorner()
	{
		std::cout << "I'm sarah corner from Terminator2" << '\n';
	}
}

int main()
{
	// UNNAMED OR ANONYMOUS NAMESPACES
	JohnCorner();

	//EXPLICIT NAMESPACES
	Terminator1::SarahCorner();
	Terminator2::SarahCorner();

	// INLINE NAMESPACES
	SarahCorner();

	return 0;
}