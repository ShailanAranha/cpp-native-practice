#include <iostream>

int main()
{
	float* ptr{ 0 };
	if (ptr)
		std::cout << "True" << '\n';
	else
		std::cout << "False" << '\n';

	// NULL PTR (Preffered method to assign null pointer)
	int* ptr1{ nullptr }; // note: ptr is still an integer pointer, just set to a null value

	return 0;
}

