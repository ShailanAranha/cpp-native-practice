#include <iostream>

int main()
{
	int l_iValue{ 5 };

	int* ptr{ &l_iValue };

	int** ptrptr{ &ptr };

	std::cout << *ptr << '\n';
	std::cout << *ptrptr << '\n';
	std::cout << **ptrptr << '\n';

	return 0;
}
