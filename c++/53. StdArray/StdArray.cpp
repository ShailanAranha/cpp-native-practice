#include <iostream>
#include <array>

int main()
{
	std::array<int, 4> myArray;

	myArray = { 0,2,3,9 };

	for(int num:myArray)
	{
		std::cout << num;
	}

	std::cout << '\n';

	// THIS WORKS ONLY IN C++17
	std::array Alphabeta{ 'd','a','q','p' };

	for (char num : Alphabeta)
	{
		std::cout << num;
	}
	std::cout << '\n';

	std::cout << Alphabeta.size();

	return 0;
}