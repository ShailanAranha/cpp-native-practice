#include <iostream>
#include <limits> // for std::numeric_limits

void ignoreLine()
{
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

int main()
{
	// EXTRACTION FAILED

std:: cout << "Enter a doube:";
double input{0.0};
std::cin >> input;


if (std::cin.fail()) // has a previous extraction failed?
{
    std::cin.clear(); // put us back in 'normal' operation mode
    ignoreLine(); // and remove the bad input
}

std::cout << "Entered value is "<< input;


	return 0;
}