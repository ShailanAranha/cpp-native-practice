#include <iostream>

bool isEven(int a_iValue);

int main()
{
	std::cout << "Enter a 0 to exit\n";
	bool l_isExit{ false };

	int l_iInput{ 0 };
	while (!l_isExit)
	{

		std::cout << "Enter a number to check if the number is even number:";

		std::cin >> l_iInput;

		if (l_iInput == 0)
		{
			l_isExit = true;
		}

		if (isEven(l_iInput))
		{
			std::cout << "The number is even \n";
		}
		else
		{
			std::cout << "The number is odd \n";
		}
	}


	return 0;
}

bool isEven(int a_iValue)
{
	int l_iRemiander = a_iValue % 2;

	if (l_iRemiander == 0)
	{
		return true;
	}

	return false;
}