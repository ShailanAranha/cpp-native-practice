#include <iostream>

class University
{
private: 
    int Id{};
    std::string Name{};

public :
    int getId()
    {
        return Id;
    }

private:
    void PriovteMethod()
    {
        std::cout << "private metjod called" << '\n';
    }

    // de4fault constructer
public:
    University()
    {
        //Id = 0123; // some weird result
        Id = 1123;
        Name = "Default name";
    }

    // parameterised coinstructor

    University(int a_iId, std::string a_strName)
    {
        Id = a_iId;
        Name = a_strName;
    }
};

int main()
{
    University m_city{};

    std::cout << m_city.getId() << '\n';

    University m_cass{789, "cass"};

    std::cout << m_cass.getId() << '\n';

   return 0;
}

